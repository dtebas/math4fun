package com.example.leeii.funmath;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
    }

    public void goToContagem(View view) {
        Intent intent = new Intent(this, Contagem.class);
        startActivity(intent);

    }

    public void goToAritmeticaBasica(View view) {
        Intent intent = new Intent(this, AritmeticaBasica.class);
        startActivity(intent);

    }

    public void goToMaiorNumero(View view) {
        Intent intent = new Intent(this, MaiorNumero.class);
        startActivity(intent);

    }
}

package com.example.leeii.funmath;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.Random;

public class Contagem extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton rbt1;
    private RadioButton rbt2;
    private RadioButton rbt3;
    private TextView pergunta;
    private ImageView imagemSaida;
    private TextView title;
    private Button bt_next;
    private AlertDialog alerta;


    int num, acertos = 0, count = 0;

    Integer[] numeros = new Integer[5];
    Integer[] alternativas = new Integer[2];
    Integer[] respostas = {1, 3, 4, 5, 7, 10, 13, 15, 16, 19};
    Integer[] imageID = {
            R.drawable.bb8,
            R.drawable.copos,
            R.drawable.canetas,
            R.drawable.lighsabers,
            R.drawable.dados,
            R.drawable.portas,
            R.drawable.pratos,
            R.drawable.tenis,
            R.drawable.oculos,
            R.drawable.calabresas
    };

    String[] nomes = {"Robô", "Copos", "Canetas", "Sabes de Luz", "Dados", "Portas", "Pratos", "Tênis", "Óculos", "Calabresas"};
    String[] perguntas = {
        "Qual a quantidade de robôs nessa imagem?",
        "Qual a quantidade de copos nessa imagem?",
        "Qual a quantidade de canetas nessa imagem?",
        "Qual a quantidade de Sabres de Luz nessa imagem?",
        "Qual a quantidade de dados nessa imagem?",
        "Qual a quantidade de portas nessa imagem?",
        "Qual a quantidade de pratos nessa imagem?",
        "Qual a quantidade de pares de tênis nessa imagem?",
        "Qual a quantidade de óculos nessa imagem?",
        "Qual a quantidade de calabresas nessa imagem?"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_contagem);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
    }

    public void onClear(View view) {
        radioGroup.clearCheck();
    }

    public void mostraResposta (String resposta){
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogTheme);
        //builder.setTitle("Inserir resultado aqui");
        if (resposta == respostas[num].toString()) {
            builder.setMessage("Parabéns, você acertou!");
            acertos++;
        }

        else {
            builder.setMessage("Infelizmente você errou... Há " + respostas[num]+ " " + nomes[num]);
        }

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        alerta = builder.create();
        alerta.show();
    }

    public int randomNumber(int bound) {
        Random r = new Random();
        int number = r.nextInt(bound);
        return number;
    }

    public void onClick (View view) {
        rbt1 = (RadioButton)findViewById(R.id.radioButton1);
        rbt2 = (RadioButton)findViewById(R.id.radioButton2);
        rbt3 = (RadioButton)findViewById(R.id.radioButton3);

        pergunta = (TextView)findViewById(R.id.enunciado);
        imagemSaida = (ImageView)findViewById(R.id.imagem);
        title = (TextView)findViewById(R.id.game1Explain);
        bt_next = (Button)findViewById(R.id.button_next);
        bt_next.setText("Responder");

        RadioButton rb = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());

        if (radioGroup.getCheckedRadioButtonId() == -1 && count > 0 && count < 6) {
            Toast.makeText(getApplicationContext(), "Marque uma opção!", Toast.LENGTH_SHORT).show();
        }

        else if (count == 5) {
                mostraResposta(rb.getText().toString());
                onClear(view);
                imagemSaida.setVisibility(View.INVISIBLE);
                rbt1.setVisibility(View.INVISIBLE);
                rbt2.setVisibility(View.INVISIBLE);
                rbt3.setVisibility(View.INVISIBLE);
                pergunta.setText("");
                title.setText("Está preparado para ver a sua nota?");
                bt_next.setText("Ver resultado");
                count++;
            }

            else if (count == 6) {
                bt_next.setText("");
                goToResults(view);
            }

            else if (count == 0) {
                num = randomNumber(10);
                salvaNumero();
                mostraPergunta();
                count++;
            }

            else {
                mostraResposta(rb.getText().toString());
                radioGroup.clearCheck();
                num = randomNumber(10);
                num = repeatinator(num);
                salvaNumero();
                mostraPergunta();
                count++;
            }
    }


    public int repeatinator(int num) {
        for (int i = 0; i < count; i++) {
            if (numeros[i] == num) {
                num = randomNumber(10);
                i = 0;
            }

        }
        return num;
    }


    public void mostraPergunta() {
        title.setText("");
        mostraOptions();
        imagemSaida.setVisibility(View.VISIBLE);
        imagemSaida.setImageResource(imageID[num]);
        pergunta.setText((count + 1) + ". " + perguntas[num]);
    }

    public void mostraOptions () {
        int contador = 0;
        int idRadioButton = randomNumber(3);
        rbt1.setVisibility(View.VISIBLE);
        rbt2.setVisibility(View.VISIBLE);
        rbt3.setVisibility(View.VISIBLE);

        switch (idRadioButton) {
            case 0:
                rbt1.setText(respostas[num].toString());
                rbt2.setText(String.valueOf(respostas[num] + 1));
                rbt3.setText(String.valueOf(respostas[num] - 1));
                break;
            case 1:
                rbt2.setText(respostas[num].toString());
                rbt1.setText(String.valueOf(respostas[num] + 1));
                rbt3.setText(String.valueOf(respostas[num] - 1));
                break;
            case 2:
                rbt3.setText(respostas[num].toString());
                rbt2.setText(String.valueOf(respostas[num] + 1));
                rbt1.setText(String.valueOf(respostas[num] - 1));
                break;
        }
    }

    public void salvaNumero() {
        if(numeros.length > count)
            numeros[count] = num;
    }

    public void goToResults(View view) {
        Intent intent = new Intent(this, ResultActivity.class);
        Bundle params = new Bundle();
        params.putInt("nota", acertos);
        intent.putExtras(params);
        finishAffinity();
        startActivity(intent);
        finish();
    }

}

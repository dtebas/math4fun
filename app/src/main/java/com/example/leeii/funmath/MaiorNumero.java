package com.example.leeii.funmath;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class MaiorNumero extends AppCompatActivity {

    private EditText inputResposta;
    private TextView title, equation, pergunta;
    private Button bt_next;
    private AlertDialog alerta;

    Integer[] x = new Integer[3];
    String[] y = new String[3];

    int num1, num2, num3, acertos = 0, count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_maior_numero);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
    }

    public void mostraResposta (){
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogTheme);



       if (Integer.parseInt(inputResposta.getText().toString()) == Integer.parseInt(String.valueOf(y[0]+y[1]+y[2]))) {
            builder.setMessage("Parabéns, você acertou!");
            acertos++;
       }

       else {
          builder.setMessage("Infelizmente você errou... A resposta correta é: " + y[0]+y[1]+y[2]);
       }

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                inputResposta.getText().clear();
            }
        });
        alerta = builder.create();
        alerta.show();
    }

    public int randomNumber(int bound) {
        Random r = new Random();
        int number = r.nextInt(bound);
        return number;
    }

    public void onClick (View view) {
        inputResposta = (EditText)findViewById(R.id.input);
        pergunta = (TextView)findViewById(R.id.enunciado);
        equation = (TextView)findViewById(R.id.equation);
        title = (TextView)findViewById(R.id.game3Explain);
        bt_next = (Button)findViewById(R.id.button_next);
        bt_next.setText("Responder");


        if (inputResposta.length() == 0 && count > 0 && count < 6) {
            Toast.makeText(getApplicationContext(), "Preencha a resposta!", Toast.LENGTH_SHORT).show();
        }

        else if (inputResposta.length() > 3) {
            Toast.makeText(getApplicationContext(), "Número muito grande", Toast.LENGTH_SHORT).show();
        }

        else if (count == 5) {
            mostraResposta();
            equation.setText("");
            inputResposta.setVisibility(View.INVISIBLE);
            pergunta.setText("");

            title.setText("Está preparado para ver a sua nota?");
            bt_next.setText("Ver resultado");
            count++;
        }

        else if (count == 6) {
            inputResposta.setVisibility(View.INVISIBLE);
            bt_next.setText("");
            goToResults(view);
        }

        else if (count == 0) {
            pergunta.setText((count + 1) + ". " + "Qual o maior inteiro usando a combinação desses inteiros?");
            mostraPergunta();
            count++;
            inputResposta.setVisibility(View.VISIBLE);
        }

        else {
            pergunta.setText((count + 1) + ". " + "Qual o maior inteiro usando a combinação desses inteiros?");
            inputResposta.setVisibility(View.VISIBLE);
            mostraResposta();
            mostraPergunta();
            count++;
        }
    }

    public void mostraPergunta() {
        title.setText("");
        num1 = randomNumber(10);
        num2 = randomNumber(10);
        num3 = randomNumber(10);
        resultado(num1, num2, num3);

        equation.setText(String.valueOf(num1) + " " + String.valueOf(num2) + " " + String.valueOf(num3) + " ");
    }

    public void goToResults(View view) {
        Intent intent = new Intent(this, ResultActivity.class);
        Bundle params = new Bundle();
        params.putInt("nota", acertos);
        intent.putExtras(params);
        finishAffinity();
        startActivity(intent);
        finish();
    }

    public int maior (int x, int y) {
        if (x > y)
            return x;
        else
            return y;
    }

    public int menor (int x, int y) {
        if (x > y)
            return y;
        else
            return x;
    }


    public void resultado(int num1, int num2, int num3) {
            int aux;
            x[0] = maior(num1, num2);
            aux = menor(num1, num2);
            if (x[0] > num3) {
                x[1] = maior(aux, num3);
                x[2] = menor(aux, num3);
            }

            else {
                x[0] = num3;
                x[1] = maior(num1, num2);
                x[2] = menor(num1, num2);
            }

            for (int i = 0; i < 3; i ++) {
                y[i] = x[i].toString();
            }



    }
}
